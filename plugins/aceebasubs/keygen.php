<?php
/**
 * @package        akeebasubs
 * @copyright    Copyright (c)2010-2012 Nicholas K. Dionysopoulos / AkeebaBackup.com
 * @license        GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
 */

defined('_JEXEC') or die();

require_once JPATH_ROOT . '/administrator/components/com_keygen/helpers/keygen.api.php';

class plgAkeebasubsKeygen extends JPlugin
{
    private $maxKeys = array();

    public function __construct(&$subject, $name, $config = array())
    {
        parent::__construct($subject, $name, $config);

        $this->loadLanguage();

        $this->loadLevelAssignments();
    }

    /**
     * Вывод в настройках уровня подписки, устанавливает макимальное кол-во ключей на подписку.
     *
     * @param   AkeebasubsTableLevel $level  The subscription level
     *
     * @return  object  Definition object, with two properties: 'title' and 'html'
     */
    public function onSubscriptionLevelFormRender(AkeebasubsTableLevel $level)
    {
        JLoader::import('joomla.filesystem.file');
        $filename = dirname(__FILE__) . '/override/default.php';
        if (!JFile::exists($filename)) {
            $filename = dirname(__FILE__) . '/tmpl/default.php';
        }

        if (!property_exists($level->params, 'keygen_maxKeys')) {
            $level->params->keygen_maxKeys = 0;
        }

        @ob_start();
        include_once $filename;
        $html = @ob_get_clean();

        $ret = (object)array(
            'title' => JText::_('PLG_ASUBS_KEYGEN_TAB_TITLE'),
            'html' => $html
        );

        return $ret;
    }

    /**
     * Вывод в форму оформления подписки
     *
     * @param  array $cache
     *
     * @return  array  The custom fields definitions
     */
    public function onSubscriptionFormRenderPerSubFields($cache)
    {
        $fields = array();

        // Make sure we have a level
        if (!array_key_exists('subscriptionlevel', $cache)) {
            return $fields;
        }

        // Make sure this leve supports slave subscriptions
        $level = $cache['subscriptionlevel'];
        if (!array_key_exists($level, $this->maxKeys)) {
            return $fields;
        }

        $maxKeys = $this->maxKeys[$level];

        if ($maxKeys <= 0) {
            return $fields;
        }

        $javascript_fetch = '';
        $javascript_validate = '';

        for ($i = 0; $i < $maxKeys; $i++) {
            if (array_key_exists('keysites', $cache['subcustom'])) {
                $allSlaves = $cache['subcustom']['keysites'];
            } else {
                $allSlaves = array();
            }

            if (array_key_exists($i, $allSlaves)) {
                $current = $allSlaves[$i];
            } else {
                $current = '';
            }

            $html = '<input placeholder="'.JText::_('PLG_ASUBS_KEYGEN_SITE_PLACEHOLDER').'" type="text" name="subcustom[keysites][' . $i . ']" id="keysite' . $i . '" value="' . htmlentities($current) . '" />';

            $userExists = false;
            if (!empty($current)) {
                $userExists = JUserHelper::getUserId($current) > 0;
            }

            // Setup the field
            $field = array(
                'id' => 'keysite' . $i,
                'label' => JText::sprintf('PLG_ASUBS_KEYGEN_SITE_LBL', $i + 1),
                'elementHTML' => $html,
                'invalidLabel' => JText::_('PLG_ASUBS_KEYGEN_SITE_INVALID_LBL'),
                'isValid' => empty($current) || $userExists
            );
            // Add the field to the return output
            $fields[] = $field;

            // Add Javascript
            $javascript_fetch .= <<<ENDJS
result.keysites[$i] = $('#keysite$i').val();

ENDJS;
            $javascript_validate .= <<<ENDJS

$('#keysite$i').parent().parent().removeClass('error').removeClass('success');
if(!response.subcustom_validation.keysite$i) {
	$('#keysite$i').parent().parent().addClass('error');
	$('#keysite{$i}_invalid').css('display','inline-block');
	thisIsValid = false;
} else {
	$('#keysite$i').parent().parent().removeClass('error');
		$('#keysite{$i}_invalid').css('display','none');
}

ENDJS;
        }

        $javascript = <<<ENDJS
(function($) {
	$(document).ready(function(){
		addToSubValidationFetchQueue(plg_akeebasubs_slavesubs_fetch);
		addToSubValidationQueue(plg_akeebasubs_slavesubs_validate);
	});
})(akeeba.jQuery);

function plg_akeebasubs_slavesubs_fetch()
{
	var result = {
		keysites: {}
	};

	(function($) {
$javascript_fetch
	})(akeeba.jQuery);

	return result;
}

function plg_akeebasubs_slavesubs_validate(response)
{
	var thisIsValid = true;
	(function($) {
$javascript_validate

		return thisIsValid;
	})(akeeba.jQuery);
}

ENDJS;
        $document = JFactory::getDocument();
        $document->addScriptDeclaration($javascript);

        return $fields;
    }

    /**
     * Performs validation of the custom fields, i.e. check that a valid
     * username (or no username) is set on each one of them.
     *
     * @param   object  $data
     *
     * @return  array subscription_custom_validation, valid
     */
    public function onValidatePerSubscription($data)
    {
        // Initialise the validation respone
        $response = array(
            'valid'								=> true,
            'subscription_custom_validation'	=> array()
        );

        // Make sure we have a subscription level ID
        if($data->id <= 0)
        {
            return $response;
        }

        // Fetch the custom data
        $subcustom = $data->subcustom;

        if (!array_key_exists($data->id, $this->maxKeys))
        {
            return $response;
        }
        $maxKeys = $this->maxKeys[$data->id];

        if($maxKeys <= 0)
        {
            return $response;
        }

        if(!array_key_exists('keysites', $subcustom))
        {
            return $response;
        }


        $validCount = 0;
        for($i = 0; $i < $maxKeys; $i++)
        {
            if(!array_key_exists($i, $subcustom['keysites']))
            {
                $response['subscription_custom_validation']['keysite'.$i] = true;
                continue;
            }
            $current = $subcustom['keysites'][$i];
            if (empty($current))
            {
                $response['subscription_custom_validation']['keysite'.$i] = true;
            }
            else
            {
                $valid = filter_var($current, FILTER_VALIDATE_URL);
                $response['subscription_custom_validation']['keysite'.$i] = $valid;
                if($valid)
                    $validCount ++;
            }

            $response['valid'] = $response['valid'] &&
                $response['subscription_custom_validation']['keysite'.$i];
        }

        if(!$validCount){
            $response['valid'] = false;
            $response['subscription_custom_validation']['keysite0'] = false;
        }

        return $response;
    }

    public function onOrderMessage($row)
    {
        $keys = $this->showKeys($row);
        $post_text = '';

        if (!empty($keys) && is_array($keys) && count($keys)) {
        $header = '<h3>' . JText::_('PLG_ASUBS_KEYGEN_KEY') . '</h3>';

            foreach($keys as $k=>$v)
            {
                $post_text .= '<p>' . $k  . ' -> ' . $v . '</p>';
            }
            $post_text = $header . $post_text;
        }

        return $post_text;
    }

    public function onAKAfterPaymentCallback($subscription)
    {
        if ($subscription->state != 'C')
            return 'fail';

        $keys = $this->genetateKeys($subscription);
        return 'ok';
    }

    private function genetateKeys($row)
    {

        $userId = $row->user_id;

        if ($userId == 0) {
            return '';
        }

        // Get the parameters of the row
        $params = $row->params;

        // No params? No need to check anything else!
        if (empty($params))
        {
            return array();
        }

        if (!is_object($params) && !is_array($params))
        {
            $params = json_decode($params, true);
        }
        else
        {
            $params = (array) $params;
        }

        // Nothing in the params array? No need to check anything else!
        if (empty($params["keysites"]) || !is_array($params["keysites"]) || !count($params["keysites"]))
        {
            return array();
        }

        $akeebasubsLevel = FOFModel::getTmpInstance('Levels', 'AkeebasubsModel')->setId($row->akeebasubs_level_id)->getItem();

        $maxKeys = $akeebasubsLevel->params["keygen_maxKeys"];
        $extName = $akeebasubsLevel->params["keygen_extName"];
        $sites = $params["keysites"];

        $expire_date = '0000-00-00';
        if ($this->params->get('enable_time_limit', 1) == 1) {
            $expire_date = date("Y-m-d h:i:s", strtotime("+" . $akeebasubsLevel->duration . " day"));
        }

        $keys = array();
        foreach($sites as $site){
            if (!filter_var($site, FILTER_VALIDATE_URL)) {
                continue;
            }

            if ($this->params->get('enable_sales', 1)) {
                $options = array(
                    'sale_id' => $row->akeebasubs_subscription_id,
                    'sale_component' => 'com_akeebasubs',
                    'extension' => $extName,
                    'allowed_amount_keys' => $maxKeys,
                    'user_id' => $userId
                );
                KeygenApi::addSale($options);
            }

            if ($this->params->get('enable_generate_key', 1)) {
                $options = array(
                    'site' => $site,
                    'extension' => $extName,
                    'expire_date' => $expire_date,
                    'user_id' => $userId
                );
                $result = KeygenApi::addKey($options);
                $key = $result['key'];
                $keys[$site] = $key;
            }
        }

        return $keys;
    }

    private function showKeys($row)
    {

        $userId = JFactory::getUser()->id;

        if ($userId == 0) {
            return '';
        }

        // Get the parameters of the row
        $params = $row->params;

        // No params? No need to check anything else!
        if (empty($params))
        {
            return array();
        }

        if (!is_object($params) && !is_array($params))
        {
            $params = json_decode($params, true);
        }
        else
        {
            $params = (array) $params;
        }

        // Nothing in the params array? No need to check anything else!
        if (empty($params["keysites"]) || !is_array($params["keysites"]) || !count($params["keysites"]))
        {
            return array();
        }

        $akeebasubsLevel = FOFModel::getTmpInstance('Levels', 'AkeebasubsModel')->setId($row->akeebasubs_level_id)->getItem();

        $extName = $akeebasubsLevel->params["keygen_extName"];
        $sites = $params["keysites"];

        $controller = new JControllerLegacy();
        $controller->addModelPath(JPATH_ADMINISTRATOR . '/components/com_keygen/models');

        $model = $controller->getModel('Key', 'KeygenModel');
        $model->addTablePath(JPATH_ADMINISTRATOR . '/components/com_keygen/tables');

        $table = $model->getTable();

        $keys = array();

        foreach($sites as $site)
        {
            if(empty($site)){
                continue;
            }

            if ($this->params->get('enable_generate_key', 1)) {
                $site = $table->filterSite($site);
                $options = array(
                    'site' => $site,
                    'extension' => $extName,
                    'user_id' => $userId
                );
                $result = KeygenApi::getKeyRows($options);
                $key = (isset($result[0]->key)) ? $result[0]->key : '';
                $keys[$site] = $key;
            }
        }

        return $keys;
    }


    /**
     * Loads the maximum slave subscriptions assignments for each subscription
     * level.
     */
    private function loadLevelAssignments()
    {
        $this->maxKeys = array();

        $model = FOFModel::getTmpInstance('Levels','AkeebasubsModel');
        $levels = $model->getList(true);
        $slavesKey = 'keygen_maxKeys';

        if(!empty($levels)) {
            foreach($levels as $level)
            {
                $save = false;
                if(is_string($level->params)) {
                    $level->params = @json_decode($level->params);
                    if(empty($level->params)) {
                        $level->params = new stdClass();
                    }
                } elseif(empty($level->params)) {
                    continue;
                }
                if(property_exists($level->params, $slavesKey))
                {
                    $this->maxKeys[$level->akeebasubs_level_id] = $level->params->$slavesKey;
                }
            }
        }
    }
}