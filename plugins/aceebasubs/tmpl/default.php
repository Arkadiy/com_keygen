<?php defined('_JEXEC') or die(); ?>
<div class="row-fluid">
	<div class="control-group">
		<label for="params_keygen_maxKeys" class="control-label">
			<?php echo JText::_('PLG_ASUBS_KEYGEN_MAXSLAVES_TITLE'); ?>
		</label>
		<div class="controls">
			<?php echo JHtml::_('select.integerlist', 0, 20, 1, 'params[keygen_maxKeys]', array('class' => 'input-small'), $level->params->keygen_maxKeys); ?>
			<span class="help-block">
				<?php echo JText::_('PLG_ASUBS_KEYGEN_MAXSLAVES_DESCRIPTION') ?>
			</span>
		</div>
	</div>
	<div class="control-group">
		<label for="params_keygen_extName" class="control-label">
			<?php echo JText::_('PLG_ASUBS_KEYGEN_EXTNAME_TITLE'); ?>
		</label>
		<div class="controls">
            <input type="text" class="input-big" name="params[keygen_extName]" value="<?php echo $level->params->keygen_extName; ?>"/>
			<span class="help-block">
				<?php echo JText::_('PLG_ASUBS_KEYGEN_EXTNAME_DESCRIPTION') ?>
			</span>
		</div>
	</div>
</div>