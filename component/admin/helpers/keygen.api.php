<?php
// No direct access to this file
defined('_JEXEC') or die;

class KeygenApi
{
    public static function addKey($options)
    {
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $controller = new JControllerLegacy();
        $controller->addModelPath(JPATH_ADMINISTRATOR.'/components/com_keygen/models');

        $model = $controller->getModel('Key', 'KeygenModel');
        $model->addTablePath(JPATH_ADMINISTRATOR.'/components/com_keygen/tables');

        $table = $model->getTable();

        $options['site'] = $table->filterSite($options['site']);

        $date = JDate::getInstance($options['expire_date']);
        $d = $date->format('Y-m-d');

        // Construct the query
        $query->select('`key`');
        $query->from('#__keygen_keys');
        $query->where('user_id = '.$db->quote($options['user_id']));
        $query->where('site = '.$db->quote($options['site']));
        $query->where('extension = '.$db->quote($options['extension']));
        $query->where('DATE_FORMAT(`expire_date`, "%Y-%m-%d") = '.$db->quote($d));
        $db->setQuery((string)$query, 0, 1);

        // Return the result
        $key = $db->loadResult();
		if($key){
			$options['key'] = $key;
			return $options;
		}
		


        $return = $table->bind($options);

        if($return !== false){
            $table->store();
        }

        return $return;
    }

    public static function addSale($options)
    {
        $controller = new JControllerLegacy();
        $controller->addModelPath(JPATH_ADMINISTRATOR.'/components/com_keygen/models');

        $model = $controller->getModel('Key', 'KeygenModel');
        $model->addTablePath(JPATH_ADMINISTRATOR.'/components/com_keygen/tables');

        $table = $model->getTable('Sales', 'KeygenTable');

        $return = $table->bind($options);

        if($return !== false){
            $return = $table->store();
        }

        return $return;
    }

    public static function getAllKeyByUser()
    {
        // Create a new query object.
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $uid = JFactory::getUser()->id;
        // Select some fields
        $query->select('#__keygen_keys.*, #__users.username');
        // From the hello table
        $query->from('#__keygen_keys');
        $query->leftJoin('#__users ON #__users.id = #__keygen_keys.user_id');
        $query->where('#__keygen_keys.user_id = '.(int)$uid);
        $query->order('`id` DESC');
        $db->setQuery((string)$query);
        $return = $db->loadObjectList();

        return $return;
    }

    public static function getKeyRows($options)
    {
        // Create a new query object.
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        if(is_array($options) && count($options)>0){
            foreach($options as $k=>$v){
                $query->where($db->quoteName('#__keygen_keys').'.'.$db->quoteName($k).' = '.$db->quote($v));
            }
        }

        $query->select('#__keygen_keys.*, #__users.username');
        $query->from('#__keygen_keys');
        $query->leftJoin('#__users ON #__users.id = #__keygen_keys.user_id');
        $query->order('`id` DESC');
        $db->setQuery((string)$query);
        $return = $db->loadObjectList();

        return $return;
    }
}
