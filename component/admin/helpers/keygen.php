<?php
// No direct access to this file
defined('_JEXEC') or die;

abstract class KeygenHelper
{

    public static function addSubmenu($vName)
    {
        JSubMenuHelper::addEntry(
            JText::_('COM_KEYGEN_KEYS'),
            'index.php?option=com_keygen&view=keys',
            $vName == 'keys'
        );

        JSubMenuHelper::addEntry(
            JText::_('COM_KEYGEN_EXTS'),
            'index.php?option=com_keygen&view=extensions',
            $vName == 'extensions'
        );

        JSubMenuHelper::addEntry(
            JText::_('COM_KEYGEN_SALES'),
            'index.php?option=com_keygen&view=sales',
            $vName == 'sales'
        );
    }

	/**
	 * Get the actions
	 */
	public static function getActions($messageId = 0)
	{
		$user	= JFactory::getUser();
		$result	= new JObject;
 
		if (empty($messageId)) {
			$assetName = 'com_keygen';
		}
		else {
			$assetName = 'com_keygen.message.'.(int) $messageId;
		}
 
		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.delete'
		);
 
		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}
 
		return $result;
	}
}
