<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<form action="<?php echo JRoute::_('index.php?option=com_keygen&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate">
 
	<div class="span6">
		<fieldset class="form-horizontal">		
        <?php foreach($this->form->getFieldset('details') as $field): ?>
			<div class="control-group">
                <?php if($field->type != 'Hidden'){ ?>
					<div class="control-label">
						<?php echo $field->label;?> 
					</div>
					<div class="controls">
						<?php echo $field->input;?>
					</div>
                <?php }else{ ?>
                    <?php echo $field->input;?>
                <?php } ?>
			</div>	
        <?php endforeach; ?>
        <?php if($this->config->get('remote_add_keys', 0) == 1){ ?>
            <?php foreach($this->form->getFieldset('remote') as $field): ?>
			<div class="control-group">
                <?php if($field->type != 'Hidden'){ ?>
                   <div class="control-label">
						<?php echo $field->label;?> 
					</div>
					<div class="controls">
						<?php echo $field->input;?>
					</div>
                <?php }else{ ?>
                    <?php echo $field->input;?>
                <?php } ?>
			</div>	
            <?php endforeach; ?>
        <?php } ?>
		
	</div>

	<div>
		<input type="hidden" name="task" value="key.edit" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
