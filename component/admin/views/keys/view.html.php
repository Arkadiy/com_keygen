<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');

class KeygenViewKeys extends JViewLegacy
{

	function display($tpl = null) 
	{
        $this->loadHelper('keygen');

        // Load the submenu.
        KeygenHelper::addSubmenu(JRequest::getCmd('view', 'keys'));

		// Assign data to the view
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->state		= $this->get('State');
        $this->authors		= $this->get('Authors');
        $this->sites = $this->get('Sites');
        $this->extensions = $this->get('Extensions');

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            JError::raiseError(500, implode('<br />', $errors));
            return false;
        }

		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
 
		// Set the document
		$this->setDocument();
	}
 
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
        $this->loadHelper( 'keygen' );
        $canDo = KeygenHelper::getActions();

		JToolBarHelper::title(JText::_('COM_KEYGEN'), 'keygen');

        if($canDo->get('core.create')){
            JToolBarHelper::addNew('key.add', 'JTOOLBAR_NEW');
        }
        if($canDo->get('core.delete')){
            JToolBarHelper::deleteList(JText::_('COM_KEYGEN_DELETE_EXT'), 'key.remove', 'JTOOLBAR_DELETE');
        }
        if($canDo->get('core.admin')){
            JToolBarHelper::divider();
            JToolBarHelper::custom('keygenconf.editGenerator', 'edit', 'edit', JText::_('COM_KEYGEN'), false);
        }
        if($canDo->get('core.admin')){
            JToolBarHelper::divider();
            JToolBarHelper::preferences('com_keygen');
        }
	}
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_KEYGEN'));
	}
}
