<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<div id="filter-bar" class="btn-toolbar">

    <div class="filter-search btn-group pull-left">
        <label class="element-invisible" for="filter_search" aria-invalid="false"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
		<div class="btn-wrapper input-append">
			<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_CONTENT_FILTER_SEARCH_DESC'); ?>" />

			<button type="submit" class="btn hasTooltip"><i class="icon-search"></i></button>
			<button type="button" class="btn hasTooltip" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
    </div>
    <div class="pull-right btn-group hidden-phone" >
		<div class="js-stools-field-list btn-group pull-left">
        <select name="filter_extension" class="inputbox" onchange="this.form.submit()">
            <option value="">- <?php echo JText::_('COM_KEYGEN_EXT');?> -</option>
            <?php echo JHtml::_('select.options', $this->extensions, 'value', 'text', $this->state->get('filter.extension'));?>
        </select>
		</div>
		<div class="js-stools-field-list btn-group pull-right">
        <select name="filter_site" class="inputbox" onchange="this.form.submit()">
            <option value="">- <?php echo JText::_('COM_KEYGEN_SITE');?> -</option>
            <?php echo JHtml::_('select.options', $this->sites, 'value', 'text', $this->state->get('filter.site'));?>
        </select>
		</div>
		<div class="js-stools-field-list btn-group pull-right">
        <select name="filter_author_id" class="inputbox" onchange="this.form.submit()">
            <option value="">- <?php echo JText::_('COM_KEYGEN_USER');?> -</option>
            <?php echo JHtml::_('select.options', $this->authors, 'value', 'text', $this->state->get('filter.author_id'));?>
        </select>
		</div>

    </div>
 
</div>
