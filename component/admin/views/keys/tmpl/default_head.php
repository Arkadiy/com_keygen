<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<tr>
	<th width="5">
        <?php echo JText::_('ID'); ?>
	</th>
	<th width="20">
		<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->items); ?>);" />
	</th>			
	<th width="150">
        <?php echo JText::_('COM_KEYGEN_CREATE_DATE'); ?>
	</th>
	<th width="150">
        <?php echo JText::_('COM_KEYGEN_EXPIRE_DATE'); ?>
	</th>
	<th>
        <?php echo JText::_('COM_KEYGEN_SITE'); ?>
	</th>
	<th>
        <?php echo JText::_('COM_KEYGEN_KEY'); ?>
	</th>
	<th>
        <?php echo JText::_('COM_KEYGEN_EXT'); ?>
	</th>
	<th>
        <?php echo JText::_('COM_KEYGEN_USER'); ?>
	</th>
</tr>
