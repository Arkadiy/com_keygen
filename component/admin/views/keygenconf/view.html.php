<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');

class keygenViewKeygenconf extends JViewLegacy
{
	/**
	 * display method of Hello view
	 * @return void
	 */
	public function display($tpl = null) 
	{
		// get the Data
        $filename = JPATH_COMPONENT.'/function.php';

        $this->keygen = str_replace('<?php', '', file_get_contents($filename));

		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
 
		// Set the document
		$this->setDocument();
	}
 
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		JRequest::setVar('hidemainmenu', true);

        $this->loadHelper( 'keygen' );
        $canDo = KeygenHelper::getActions();

		//$canDo = KeygenHelper::getActions($this->item->id);
		JToolBarHelper::title(JText::_('COM_KEYGEN_GENERATE_FUNCTION'), 'keygen');
		// Built the actions for new and existing records.

        if($canDo->get('core.admin')){
		    JToolBarHelper::save('keygenconf.saveGenerator', 'JTOOLBAR_SAVE');
        }
		JToolBarHelper::cancel('key.cancel', 'JTOOLBAR_CANCEL');
	}
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$isNew = $this->item->id == 0;
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_KEYGEN_GENERATE_FUNCTION_2'));
		$document->addScript(JURI::root() . $this->script);
		$document->addScript(JURI::root() . "/administrator/components/com_keygen/views/key/submitbutton.js");
		JText::script('COM_KEYGEN_ERROR');
	}
}
