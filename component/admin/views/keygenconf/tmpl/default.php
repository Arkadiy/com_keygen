<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<form action="<?php echo JRoute::_('index.php?option=com_keygen&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm">
 
	<div class="row-fluid">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_KEYGEN_GENERATOR')?></legend>
			<textarea cols="500" rows="25" name="keygen" style="width: 100%"><?php echo $this->keygen ?></textarea>
	</div>
 
	<div>
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
