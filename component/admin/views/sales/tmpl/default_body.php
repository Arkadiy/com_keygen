<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td>
			<?php echo $item->id; ?>
		</td>
		<td>
			<?php echo JHtml::_('grid.id', $i, $item->id); ?>
		</td>
		<td>
			<?php echo $item->sale_component; ?>
		</td>
		<td>
			<?php echo $item->sale_id; ?>
		</td>
		<td>
			<?php echo $item->extension; ?>
		</td>
		<td>
			<?php echo $item->allowed_amount_keys; ?>
		</td>
		<td>
			<?php echo $item->username; ?>
		</td>
	</tr>
<?php endforeach; ?>
