<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');

class keygenViewSale extends JViewLegacy
{
	/**
	 * display method of Hello view
	 * @return void
	 */
	public function display($tpl = null) 
	{
		// get the Data
		$form = $this->get('Form');
		$item = $this->get('Item');
		$script = $this->get('Script');
 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form = $form;
		$this->item = $item;
		$this->script = $script;
 
		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
 
		// Set the document
		$this->setDocument();
	}
 
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		JRequest::setVar('hidemainmenu', true);

        $this->loadHelper( 'keygen' );
        $canDo = KeygenHelper::getActions();

		JToolBarHelper::title(JText::_('COM_KEYGEN_CREATE_EXT'), 'extensoin');

        if($canDo->get('core.create') || $canDo->get('core.edit')){
            JToolBarHelper::save('sale.save', 'JTOOLBAR_SAVE');
        }

		JToolBarHelper::cancel('sale.cancel', 'JTOOLBAR_CANCEL');
	}
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$isNew = $this->item->id == 0;
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_KEYGEN_CREATE_EXT_2'));
		$document->addScript(JURI::root() . $this->script);
		$document->addScript(JURI::root() . "/administrator/components/com_keygen/views/key/submitbutton.js");
		JText::script(JText::_('COM_KEYGEN_ERROR'));
	}
}
