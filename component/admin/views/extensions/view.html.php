<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');

class KeygenViewExtensions extends JViewLegacy
{

	function display($tpl = null) 
	{
        require_once JPATH_ADMINISTRATOR.'/components/com_keygen/helpers/keygen.php';

        // Load the submenu.
        KeygenHelper::addSubmenu(JRequest::getCmd('view', 'keys'));

		// Get data from the model
		$items = $this->get('Items');
		$pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;
 
		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
 
		// Set the document
		$this->setDocument();
	}
 
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
        $this->loadHelper( 'keygen' );
        $canDo = KeygenHelper::getActions();

		JToolBarHelper::title(JText::_('COM_KEYGEN_GENERATOR'), 'keygen');

        if($canDo->get('core.create')){
		    JToolBarHelper::addNew('extension.add', 'JTOOLBAR_NEW');
        }
        if($canDo->get('core.edit')){
            JToolBarHelper::editList('extension.edit', 'JTOOLBAR_EDIT');
        }
        if($canDo->get('core.delete')){
            JToolBarHelper::deleteList(JText::_('COM_KEYGEN_DELETE_EXT'), 'extension.remove', 'JTOOLBAR_DELETE');
        }
	}
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_KEYGEN_GENERATOR'));
	}
}
