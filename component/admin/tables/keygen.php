<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
 
// import Joomla table library
jimport('joomla.database.table');
 
/**
 * Hello Table class
 */
class KeygenTableKeygen extends JTable
{
	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(&$db) 
	{
		parent::__construct('#__keygen_keys', 'id', $db);
	}

    public function bind($array, $ignore = '')
    {
        $site = $this->filterSite($array['site']);

        $array['key'] = $this->generateKey(
            array(
                'site' => $site,
                'extension' => $array['extension'],
                'expire_date' => $array['expire_date']
            ),
            $array
        );

        if(empty($array['key'])){
            return false;
        }

        $date = JFactory::getDate(date('Y-m-d', strtotime('now')));
        $array['date'] = $date->toSql();

        if(parent::bind($array, $ignore)){
            return $array;
        }
        else{
            return false;
        }
    }

    public function filterSite($site){
        $site = str_replace(array('http://','https://'), '', $site);
        $site = rtrim($site, '/');
        $site = (substr($site, 0, 4) == 'www.') ? substr($site, 4) : $site;
        return $site;
    }

    /**
     * Обратимое шифрование методом "Двойного квадрата" (Reversible crypting of "Double square" method)
     * @param  String $input   Строка с исходным текстом
     * @param  bool   $decrypt Флаг для дешифрования
     * @return String          Строка с результатом Шифрования|Дешифрования
     * @author runcore
     */
    private function generateKey($options, $data=null) {
        if($data){
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('*')
                ->from('#__keygen_extension')
                ->where('ext_name = '.$db->quote($data['extension']));
            $db->setQuery((string)$query, 0, 1);
            $extension = $db->loadObject();
            if($extension->enable_remote_key == 1){
                return $this->remoteGenerateKey($options, $data, $extension);
            }
        }

        $key = '';
        require JPATH_ADMINISTRATOR.'/components/com_keygen/function.php';
        return $key;
    }


    private function remoteGenerateKey($options, $data, $extension)
    {
        $post = array('options'=>$options, 'data'=>$data, 'extension'=>$extension);
        $post = json_encode($post);
        $post = base64_encode($post);

        $site = $extension->remote_site;
        if(empty($site)){
            return false;
        }
        if(substr($site, -1) != '/'){
            $site = $site.'/';
        }
        if(strpos($site, 'http://') === false && strpos($site, 'https://') === false){
            $site = 'http://'.$site;
        }
        $url = $site.'index.php?option=com_keygen&task=key.remote_add';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_REFERER, JURI::base());
        curl_setopt($curl, CURLOPT_POSTFIELDS, 'data='.$post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        $curl_results = curl_exec ($curl);
        curl_close ($curl);

        $json = json_decode($curl_results);
        
        $key = (!empty($json->key)) ? $json->key : '';

        if(!$key)
            $this->setError($json->error);

        return $key;
    }

	/**
	 * Method to compute the default name of the asset.
	 * The default name is in the form `table_name.id`
	 * where id is the value of the primary key of the table.
	 *
	 * @return	string
	 * @since	1.6
	 */
	protected function _getAssetName()
	{
		$k = $this->_tbl_key;
		return 'com_keygen.message.'.(int) $this->$k;
	}
 
	/**
	 * Method to return the title to use for the asset table.
	 *
	 * @return	string
	 * @since	1.6
	 */
	protected function _getAssetTitle()
	{
		return $this->site;
	}
 
	/**
	 * Get the parent asset id for the record
	 *
	 * @return	int
	 * @since	1.6
	 */
	protected function _getAssetParentId($table = null, $id = null)
	{
		$asset = JTable::getInstance('Asset');
		$asset->loadByName('com_keygen');
		return $asset->id;
	}
}
