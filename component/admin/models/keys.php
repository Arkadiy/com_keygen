<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');

class KeygenModelKeys extends JModelList
{
    protected function populateState($ordering = null, $direction = null)
    {
        // Initialise variables.
        $app = JFactory::getApplication();
        $session = JFactory::getSession();

        // Adjust the context to support modal layouts.
        if ($layout = JRequest::getVar('layout')) {
            $this->context .= '.'.$layout;
        }

        $search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $site = $this->getUserStateFromRequest($this->context.'.filter.site', 'filter_site', 0, 'string');
        $this->setState('filter.site', $site);

        $authorId = $app->getUserStateFromRequest($this->context.'.filter.author_id', 'filter_author_id');
        $this->setState('filter.author_id', $authorId);

        $extension = $this->getUserStateFromRequest($this->context.'.filter.extension', 'filter_extension', '');
        $this->setState('filter.extension', $extension);

        // List state information.
        parent::populateState('a.title', 'asc');
    }

	protected function getListQuery()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('kk.*, u.username');
		$query->from('#__keygen_keys as kk');
        $query->leftJoin('#__users as u ON u.id = kk.user_id');
        $query->group('kk.id');
		$query->order('kk.id DESC');

        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = $db->Quote('%'.$db->escape($search, true).'%');
            $query->where('(kk.site LIKE '.$search.' OR kk.extension LIKE '.$search.' OR u.username LIKE '.$search.')');
        }

        $authorId = $this->getState('filter.author_id');
        if (is_numeric($authorId)) {
            $query->where('kk.user_id = '.(int) $authorId);
        }

        $site = $this->getState('filter.site');
        if (!empty($site)) {
            $query->where('kk.site = '.$db->quote($site));
        }

        $extension = $this->getState('filter.extension');
        if (!empty($extension)) {
            $query->where('kk.extension = '.$db->quote($extension));
        }

		return $query;
	}

    public function getAuthors() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Construct the query
        $query->select('u.id AS value, u.username AS text');
        $query->from('#__users AS u');
        $query->join('INNER', '#__keygen_keys AS c ON c.user_id = u.id');
        $query->group('u.id, u.name');
        $query->order('u.name');

        // Setup the query
        $db->setQuery($query->__toString());

        // Return the result
        return $db->loadObjectList();
    }

    public function getSites() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Construct the query
        $query->select('DISTINCT site AS value, site AS text');
        $query->from('#__keygen_keys');
        $query->order('site ASC');

        // Setup the query
        $db->setQuery($query->__toString());

        // Return the result
        return $db->loadObjectList();
    }

    public function getExtensions() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Construct the query
        $query->select('DISTINCT extension AS value, extension AS text');
        $query->from('#__keygen_keys');
        $query->order('extension ASC');

        // Setup the query
        $db->setQuery($query->__toString());

        // Return the result
        return $db->loadObjectList();
    }
}
