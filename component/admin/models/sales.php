<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');

class KeygenModelSales extends JModelList
{
    protected function populateState($ordering = null, $direction = null)
    {
        // Initialise variables.
        $app = JFactory::getApplication();
        $session = JFactory::getSession();

        // Adjust the context to support modal layouts.
        if ($layout = JRequest::getVar('layout')) {
            $this->context .= '.'.$layout;
        }

        $search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $authorId = $app->getUserStateFromRequest($this->context.'.filter.author_id', 'filter_author_id');
        $this->setState('filter.author_id', $authorId);

        $extension = $this->getUserStateFromRequest($this->context.'.filter.extension', 'filter_extension', '');
        $this->setState('filter.extension', $extension);

        // List state information.
        parent::populateState('a.title', 'asc');
    }

	protected function getListQuery()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('ks.*, u.username');
		$query->from('#__keygen_sales as ks');
        $query->leftJoin('#__users as u ON u.id = ks.user_id');
        $query->group('ks.id');
		$query->order('ks.id DESC');
		return $query;

        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = $db->Quote('%'.$db->escape($search, true).'%');
            $query->where('(ks.extension LIKE '.$search.' OR ks.sale_component LIKE '.$search.' OR ks.sale_id LIKE '.$search.' OR u.username LIKE '.$search.')');
        }

        $authorId = $this->getState('filter.author_id');
        if (is_numeric($authorId)) {
            $query->where('ks.user_id = '.(int) $authorId);
        }

        $extension = $this->getState('filter.extension');
        if (!empty($extension)) {
            $query->where('ks.extension = '.$db->quote($extension));
        }
	}

    public function getAuthors() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Construct the query
        $query->select('u.id AS value, u.username AS text');
        $query->from('#__users AS u');
        $query->join('INNER', '#__keygen_sales AS c ON c.user_id = u.id');
        $query->group('u.id, u.name');
        $query->order('u.name');

        // Setup the query
        $db->setQuery($query->__toString());

        // Return the result
        return $db->loadObjectList();
    }

    public function getExtensions() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Construct the query
        $query->select('DISTINCT extension AS value, extension AS text');
        $query->from('#__keygen_sales');
        $query->order('extension ASC');

        // Setup the query
        $db->setQuery($query->__toString());

        // Return the result
        return $db->loadObjectList();
    }
}
