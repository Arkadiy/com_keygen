<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

class KeygenControllerKeygenconf extends JControllerForm
{
    public function saveGenerator(){

        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        $app = JFactory::getApplication();

        $msg = '';
        $filename = JPATH_ADMINISTRATOR.'/components/com_keygen/function.php';
        $content = (!empty($_POST['keygen'])) ? stripslashes($_POST['keygen']) : '';
        $content = "<?php \n".$content;

        if (JFile::write($filename, $content) === FALSE) {
            $msg = JText::_('COM_KEYGEN_NO_WRITE_FILE');
        }
        else{
            $msg = JText::_('COM_KEYGEN_WRITE_FILE_OK');
        }

        $app->redirect('index.php?option=com_keygen', $msg);
    }

    public function editGenerator(){
        JRequest::setVar( 'view', 'keygenconf' );
        parent::display();
    }
}
