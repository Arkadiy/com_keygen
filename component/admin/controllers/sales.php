<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');

class KeygenControllerSales extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 * @since	1.6     KeygenModelKey
	 */
	public function getModel($name = 'Sales', $prefix = 'KeygenModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
}
