<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

class KeygenControllerSale extends JControllerForm
{
    public function remove(){
        $input = new JInput();
        $app = JFactory::getApplication();
        $ids = $input->get('cid', array(), 'ARRAY');
        $model = $this->getModel();
        $table = $model->getTable();

        if(count($ids) > 0){
            foreach($ids as $id){
                 $table->delete((int)$id);
            }
        }

        $app->redirect('index.php?option=com_keygen&view=sales');
    }
}
