DROP TABLE IF EXISTS `#__keygen_keys`;
 
CREATE TABLE `#__keygen_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL,
  `site` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `expire_date` date NOT NULL,
  `extension` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
   PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__keygen_extension`;

CREATE TABLE `#__keygen_extension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `ext_name` varchar(100) NOT NULL,
   PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__keygen_sales`;
	
CREATE TABLE `#__keygen_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` varchar(100) NOT NULL,
  `sale_component` varchar(100) NOT NULL,
  `extension` varchar(100) NOT NULL,
  `allowed_amount_keys` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
   PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

