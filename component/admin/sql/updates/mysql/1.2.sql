DROP TABLE IF EXISTS `#__keygen_sales`;
	
CREATE TABLE `#__keygen_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` varchar(100) NOT NULL,
  `sale_component` varchar(100) NOT NULL,
  `extension` varchar(100) NOT NULL,
  `allowed_amount_keys` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
   PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;