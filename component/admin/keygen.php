<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_keygen'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}
 
// require helper file
JLoader::register('KeygenHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'keygen.php');
 
// import joomla controller library
jimport('joomla.application.component.controller');

$controller = JControllerLegacy::getInstance('Keygen');

// Perform the Request task
$controller->execute(JFactory::getApplication()->input->getCmd('task', ''));
 
// Redirect if set by the controller
$controller->redirect();
