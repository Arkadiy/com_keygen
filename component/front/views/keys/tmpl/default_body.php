<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php
foreach($this->items as $i => $item):
    if($item->expire_date == '' || $item->expire_date == '0000-00-00'){
        $item->expire_date = JText::_('COM_KEYGEN_NEWER');
    }

    ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td>
			<?php echo $item->date; ?>
		</td>
		<td>
			<?php echo $item->expire_date; ?>
		</td>
		<td>
			<?php echo $item->site; ?>
		</td>
		<td>
			<?php echo $item->key; ?>
		</td>
		<td>
			<?php echo $item->extension; ?>
		</td>
	</tr>
<?php endforeach; ?>
