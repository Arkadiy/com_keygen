<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
if(count($this->items)){

}
?>
    <h2><?php echo JText::_('COM_KEYGEN_USER_KEYS');?></h2>
	<table class="keys table table-striped" style="width: 100%">
		<thead><?php echo $this->loadTemplate('head');?></thead>
		<tfoot><?php echo $this->loadTemplate('foot');?></tfoot>
		<tbody><?php echo $this->loadTemplate('body');?></tbody>
	</table>
<?php if($this->front_add_keys) : ?>
    <div class="addKey">
        <input
                type="button"
                class="button"
                value="<?php echo JText::_('COM_KEYGEN_ADD_KEY');?>"
                onclick="document.location.href='<?php echo JRoute::_('index.php?option=com_keygen&view=key');?>'"
                />
    </div>
<?php endif; ?>
