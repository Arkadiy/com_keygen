<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<tr>
	<th>
        <?php echo JText::_('COM_KEYGEN_CREATE_DATE'); ?>
	</th>
	<th>
        <?php echo JText::_('COM_KEYGEN_EXPIRE_DATE'); ?>
	</th>
	<th>
        <?php echo JText::_('COM_KEYGEN_SITE'); ?>
	</th>
	<th>
        <?php echo JText::_('COM_KEYGEN_KEY'); ?>
	</th>
	<th>
        <?php echo JText::_('COM_KEYGEN_EXT'); ?>
	</th>
</tr>
