<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
if(count($this->items)){

}
?>
    <h2><?php echo JText::_('COM_KEYGEN_ADD_KEYS');?></h2>
	<form name="adminForm" id="adminForm" method="post" action="/">
        <table>
            <tr>
                <td><label for="extension"><?php echo JText::_('COM_KEYGEN_SELECT_EXT');?></label></td>
                <td><?php echo $this->selectExt; ?></td>
            </tr>
            <tr>
                <td><label for="site"><?php echo JText::_('COM_KEYGEN_INSERT_SITE');?></label></td>
                <td><input type="text" name="site" id="site" value="" class="inputbox" size="50"/></td>
            </tr>
        </table>
        <div class="addKey">
            <input
                    type="button"
                    class="button"
                    value="<?php echo JText::_('COM_KEYGEN_ADD_KEY');?>"
                    onclick="ComKeygenSubmitForm()"
                    />
        </div>
        <input type="hidden" name="option" value="com_keygen"/>
        <input type="hidden" name="task" value="key.add"/>
	</form>

