<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');

class KeygenViewKey extends JViewLegacy
{

	function display($tpl = null) 
	{
        $app = JFactory::getApplication();

        JFactory::getDocument()->addScript('/components/com_keygen/assets/js/keygen.js');

		// Get data from the model
		$AllowedAddKeys = $this->get('AllowedAddKeys');

        if(!$AllowedAddKeys){
           $app->redirect(JRoute::_('index.php?option=com_keygen&view=keys'), JText::_('COM_KEYGEN_NO_ALLOWED_KEYS'), 'error');
        }

        $this->selectExt = JHtml::_('select.genericlist', $AllowedAddKeys, 'extension', '', 'value', 'text');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

 

 
		// Display the template
		parent::display($tpl);
	}
}
