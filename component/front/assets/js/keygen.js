/**
 * Created with JetBrains PhpStorm.
 * User: Аркадий
 * Date: 29.01.13
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */

function ComKeygenSubmitForm(){
    var site = document.getElementById('site').value;
    getUrl('/index.php?option=com_keygen&task=key.validate&ajax=1&site='+site);

}

function submitKeyForm(resp){
    if(resp == 1){
        document.forms.adminForm.submit();
    }
    else{
        alert('Please enter a valid site');
    }
}

function getXmlHttp(){
    try {
        return new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            return new ActiveXObject("Microsoft.XMLHTTP");
        } catch (ee) {
        }
    }
    if (typeof XMLHttpRequest!='undefined') {
        return new XMLHttpRequest();
    }
}

// Получить данные с url и вызывать cb - коллбэк c ответом сервера
function getUrl(url) {
var xmlhttp = getXmlHttp();
// IE кэширует XMLHttpRequest запросы, так что добавляем случайный параметр к URL
// (хотя можно обойтись правильными заголовками на сервере)
xmlhttp.open("GET", url+'&r='+Math.random());
xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4) {
        var resp = xmlhttp.responseText;
        submitKeyForm(resp);
    }
}
xmlhttp.send(null);
}
