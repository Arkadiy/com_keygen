<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');

class KeygenControllerKey extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 * @since	1.6     KeygenModelKey
	 */
	public function validate($site=null)
	{
        $input = new JInput();
        $ajax = $input->getInt('ajax', 0);

        if(!$site){
            $site = $input->getString('site', '');
        }

        if(filter_var($site, FILTER_VALIDATE_URL))
        {
            $return = 1;
        }
        else{
            $return = 0;
        }

        if($ajax){

            Header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); //Дата в прошлом
            Header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            Header("Pragma: no-cache"); // HTTP/1.1
            Header("Last-Modified: ".gmdate("D, d M Y H:i:s")."GMT");

            echo $return;
            die;
        }
        else{
            return $return;
        }
	}

    public function validateExtension($extension)
	{
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('COUNT(*)')
            ->from('#__keygen_extension')
            ->where('`password` = '.$db->quote($extension->password))
            ->where('`ext_name` = '.$db->quote($extension->ext_name));
        $db->setQuery((string)$query);
        return (int)$db->loadResult();
	}

    private function validateSite()
	{
        $config = JComponentHelper::getParams('com_keygen');
        $sites = $config->get('remote_hosts', '');

        if(!$sites)
            return false;

        $sites = explode('\n', $sites);
        if(!count($sites))
            return false;

        $ref = $this->clleanUrl($_SERVER['HTTP_REFERER']);

        foreach($sites as $site){
            if(strpos($site, $ref) !== false){
                return true;
            }
        }

        return false;
	}

    private function clleanUrl($url){
        $url = str_replace(array('http://', 'https://'), '', $url);
        $url = explode('/', $url);
        $url = $url[0];
        return $url;
    }


	public function add()
	{
        $input = new JInput();
        $userId = JFactory::getUser()->id;
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        $front_add_keys = JComponentHelper::getParams('com_keygen')->get('front_add_keys', 1);

        if(!$front_add_keys){
            $app->redirect(JRoute::_('index.php?option=com_keygen&view=keys'), JText::_('COM_KEYGEN_CONF_DISABLE_ADD_KEY'), 'error');
        }

        $site = $input->getString('site', '');
        $extension = $input->getString('extension', '');

        if(!$this->validate()){
           $app->redirect(JRoute::_('index.php?option=com_keygen&view=key'), JText::_('COM_KEYGEN_SITE_INVALID'), 'error');
        }

        $query = $db->getQuery(true);
        $query->select('COUNT(*)');
        $query->from('#__keygen_extension');
        $query->where('ext_name = '.$db->quote($extension));
        $db->setQuery((string)$query);
        $res = $db->loadResult();

        if(!$res){
            $app->redirect(JRoute::_('index.php?option=com_keygen&view=key'), JText::_('COM_KEYGEN_EXT_INVALID'), 'error');
        }

        require_once JPATH_BASE . '/administrator/components/com_keygen/helpers/keygen.api.php';

        $options = array(
            'site' => $site,
            'extension' => $extension,
            'user_id' => $userId
        );

        if(KeygenApi::addKey($options)){
            $app->redirect(JRoute::_('index.php?option=com_keygen&view=key'), JText::_('COM_KEYGEN_KEY_ADDED'));
        }
        else{
            $app->redirect(JRoute::_('index.php?option=com_keygen&view=key'), JText::_('COM_KEYGEN_KEY_ADD_FAIL'), 'error');
        }
	}

    public function remote_add()
	{
        $input = new JInput();
        $app = JFactory::getApplication();

        $return = array('error'=>'', 'key'=>'');
        $remote_add_keys = JComponentHelper::getParams('com_keygen')->get('remote_add_keys', 0);

        if(!$remote_add_keys){
            $return['error'] = JText::_('COM_KEYGEN_CONF_DISABLE_ADD_KEY');
            header('Content-type: application/json');
            echo json_encode($return);
            die;
        }

        if(!$this->validateSite()){
            $return['error'] = JText::_('COM_KEYGEN_REFERER_SITE_NOT_ALLOWED');
            header('Content-type: application/json');
            echo json_encode($return);
            die;
        }

        $post = $input->getBase64('data', '');
        $post = base64_decode($post);
        $post = json_decode($post);
        if(!$post){
            $return['error'] = JText::_('COM_KEYGEN_BAD_POST');
            header('Content-type: application/json');
            echo json_encode($return);
            die;
        }

        $site = $post->data->site;
        $extension = $post->extension->ext_name;
        $userId = 0;

        if(!$this->validate($site)){
            $return['error'] = JText::_('COM_KEYGEN_SITE_INVALID');
            header('Content-type: application/json');
            echo json_encode($return);
            die;
        }

        if(!$this->validateExtension($post->extension)){
            $return['error'] = JText::_('COM_KEYGEN_EXT_INVALID');
            header('Content-type: application/json');
            echo json_encode($return);
            die;
        }

        require_once JPATH_BASE . '/administrator/components/com_keygen/helpers/keygen.api.php';

        JTable::addIncludePath(JPATH_ROOT . '/administrator/components/com_keygen/tables');
        $table = JTable::getInstance('Keygen', 'KeygenTable');
        $site = $table->filterSite($site);

        $options = array(
            'site' => $site,
            'extension' => $extension,
            'user_id' => $userId
        );

        if(KeygenApi::addKey($options)){
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);



            // Construct the query
            $query->select('`key`');
            $query->from('#__keygen_keys');
            $query->where('user_id = '.$db->quote($userId));
            $query->where('site = '.$db->quote($site));
            $query->where('extension = '.$db->quote($extension));

            $db->setQuery((string)$query, 0, 1);

            // Return the result
            $key = $db->loadResult();

            if(!$key){
                $return['error'] = JText::_('COM_KEYGEN_GENERATE_KEY_ERROR');
                header('Content-type: application/json');
                echo json_encode($return);
                die;
            }
            else{
                $return['key'] = $key;
                header('Content-type: application/json');
                echo json_encode($return);
                die;
            }
        }
        else{
            $return['error'] = JText::_('COM_KEYGEN_GENERATE_KEY_ERROR');
            header('Content-type: application/json');
            echo json_encode($return);
            die;
        }
	}
}
