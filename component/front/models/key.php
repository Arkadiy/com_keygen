<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelform library
jimport('joomla.application.component.modeladmin');
 

class KeygenModelKey extends JModelAdmin
{
    public function getAllowedAddKeys()
    {
        $db = JFactory::getDbo();
        $userId = JFactory::getUser()->id;

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__keygen_sales');
        $query->where('user_id = '.(int)$userId);
        $db->setQuery((string)$query);
        $sales = $db->loadObjectList();

        if(!is_array($sales) || count($sales) == 0){
            return false;
        }

        $allowedKeys = array();
        foreach($sales as $sale){
            $amount = (!empty($allowedKeys[$sale->extension])) ? $allowedKeys[$sale->extension] : 0;
            $allowedKeys[$sale->extension] = $amount + $sale->allowed_amount_keys;
        }

        $query = $db->getQuery(true);
        $query->select('e.ext_name, e.name, COUNT(k.extension) as amount');
        $query->from('#__keygen_keys as k');
        $query->leftJoin('#__keygen_extension as e ON (k.extension = e.ext_name AND k.user_id = '.(int)$userId.')');
        $query->where('k.user_id = '.(int)$userId);
        $query->group('e.id');
        $db->setQuery((string)$query);
        $extensions = $db->loadObjectList();

        $options = array();
        foreach($extensions as $extension){
            if(isset($allowedKeys[$extension->ext_name]) && $allowedKeys[$extension->ext_name] > $extension->amount){
                $i = $allowedKeys[$extension->ext_name] - $extension->amount;
                $options[] = JHtml::_('select.option', $extension->ext_name, $extension->name.' - '.$i.' '.JText::_('COM_KEYGEN_KEYS_ALLOWED_ADD'));
            }

        }


        if(count($options) == 0){
            return false;
        }

        return $options;
    }

	/**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param	array	$data	An array of input data.
	 * @param	string	$key	The name of the key for the primary key.
	 *
	 * @return	boolean
	 * @since	1.6
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// Check specific edit permission then general edit permission.
		return JFactory::getUser()->authorise('core.edit', 'com_keygen.message.'.((int) isset($data[$key]) ? $data[$key] : 0)) or parent::allowEdit($data, $key);
	}
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Keygen', $prefix = 'KeygenTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_keygen.key', 'key', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}
		return $form;
	}
	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript()
	{
		return 'administrator/components/com_keygen/models/forms/key.js';
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_keygen.edit.key.data', array());
		if (empty($data))
		{
			$data = $this->getItem();
		}
		return $data;
	}
}
